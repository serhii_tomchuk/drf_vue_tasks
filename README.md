# TEST_TASK
---
---
### Full stack developer (Python, Vue Js)
---
---
Create a simple one-page to-do web application with the following functionality:
- simple login page (no registration needed), auth solution up to candidate;
- one simple page where we can see a list of tasks and there should be a simple form for adding a new task
    - a task should have a caption and a body where you can put some description.
---
Technical requirements:
    - backend Django/DRF
    - frontend Vue.js
         - you may use Django Templates + Vue.js or Django Rest API + separate Vue.js frontend, you may choose what is more preferable for you
    - tasks should be persisted into a database (you may choose any)
    - the solution should be packed into a docker container
    - please provide access to the Git repository with a final solution
---
### How to run the project:
---
1 - **Clone and run the project**:
```bash 
git clone git@gitlab.com:serhii_tomchuk/drf_vue_tasks.git \
        && cd drf_vue_tasks && make run
```
---
2 - **Down the project**:
```bash
docker compose down
```
---
3 - **Kill the project**:
```bash
make hardkill
```
4 - **More commands**:
```bash
make help
```
---
