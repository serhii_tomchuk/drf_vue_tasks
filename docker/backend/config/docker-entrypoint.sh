#!/bin/bash
##############################################################################
source /home/dockeruser/venv/bin/activate
cd /home/dockeruser/app
##############################################################################
set -e
##############################################################################
if [ "$1" = 'db' ]; then
    echo "Waiting for PostgreSQL..."

    while ! nc -z "$SQL_HOST" "$SQL_PORT"; do
        sleep 1
        echo "Waiting for PostgreSQL..."
    done
fi
##############################################################################
echo "PostgreSQL started"
#############################################################################
#echo "Starting migrations..."
#python3 manage.py makemigrations || echo "No migrations to apply"
#echo "Migrating..."
#python3 manage.py migrate || echo "No migrations to apply"
#echo "Migrations done"
#echo "Starting server..."
#python3 manage.py runserver 0.0.0.0:8000
#echo "Server started"
##############################################################################
exec "$@"
###END########################################################################