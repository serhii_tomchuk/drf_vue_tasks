DO
$$
-- Declare variables
DECLARE _user TEXT;
DECLARE _password TEXT;
DECLARE _db TEXT;
DECLARE _host TEXT;
DECLARE _role TEXT;
---------------------------------------------------|
BEGIN
-- Set variables
    _user := '_superuser';
    _password := '23cnx~yq3#nC4nvnt^Djcz7r#hwt%5';
    _db := 'testtaskdb';
    _host := 'db';
    _role := _user;
---------------------------------------------------|
-- Create database and user if not exists
CREATE DATABASE _db;
       WITH OWNER _role ENCODING 'UTF8' LC_COLLATE 'en_US.utf8' LC_CTYPE 'en_US.utf8' TEMPLATE template0;
-- Create role if not exists
CREATE ROLE _role;
-- Set role as superuser
ALTER ROLE _role WITH SUPERUSER;
-- Set database owner
ALTER DATABASE _db OWNER TO _role;
-- Create user if not exists
CREATE ROLE _user LOGIN ENCRYPTED _password;
-- Grant privileges to user
GRANT ALL PRIVILEGES ON DATABASE _db TO _user;
-- Grant privileges to user
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO _user;
-- Grant privileges to user
CREATE HOST _host ALL _user md5;
---------------------------------------------------|
END $$;--------------------------------------------|
------------------------END------------------------|