#!/bin/sh


GREEN='\033[0;32m'
RED='\033[0;31m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
CYAN='\033[0;36m'
RESET='\033[0m'

# Markdown
BOLD="\033[1m"
ITALIC="\033[3m"
UNDERLINE="\033[4m"
STRIKETHROUGH="\033[9m"

echo "${BLUE}#############################################################################################################${RESET}"
echo "              ${CYAN}${UNDERLINE}This will remove all containers, images, networks, volumes, pycache and pytest_cache${RESET}"
echo "                             ${RED}${BOLD}WARNING: USE IT ONLY IF YOU KNOW WHAT YOU ARE DOING!"
echo "                             THIS WILL REMOVE ALL DOCKER ITEMS IN YOUR SYSTEM OR SERVER!${RESET}"
echo "${BLUE}#############################################################################################################${RESET}"
echo "${RED}${BOLD}ARE YOU SURE?${RESET}"
read -p "[Y/N]" -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo "${RED}ABORTING.${RESET}"
    exit 1
fi
sleep 1
echo "${BLUE}#############################################################################################################${RESET}"
echo "                                        ${GREEN}Stopping all containers${RESET}"
echo "${BLUE}#############################################################################################################${RESET}"
docker stop $(docker ps -a -q) || echo "${YELLOW}No containers to stop.${RESET}"

sleep 1
echo "${BLUE}#############################################################################################################${RESET}"
echo "                                        ${GREEN}Removing all containers${RESET}"
echo "${BLUE}#############################################################################################################${RESET}"
docker rm $(docker ps -a -q) || echo "${YELLOW}No containers to remove.${RESET}"

sleep 1
echo "${BLUE}#############################################################################################################${RESET}"
echo "                                        ${GREEN}Removing all images${RESET}"
echo "${BLUE}#############################################################################################################${RESET}"
docker rmi $(docker images -a -q) || echo "${YELLOW}No images to remove.${RESET}"

sleep 1
echo "${BLUE}#############################################################################################################${RESET}"
echo "                                        ${GREEN}Removing all volumes${RESET}"
echo "${BLUE}#############################################################################################################${RESET}"
docker volume rm $(docker volume ls -q) || echo "${YELLOW}No volumes to remove.${RESET}"

sleep 1
echo "${BLUE}#############################################################################################################${RESET}"
echo "                                        ${GREEN}Removing all networks${RESET}"
echo "${GREEN}Removing all networks${RESET}"
echo "${BLUE}#############################################################################################################${RESET}"
docker network rm $(docker network ls -q) || echo "${YELLOW}No networks to remove.${RESET}"

sleep 1
echo "${BLUE}#############################################################################################################${RESET}"
echo "                                        ${GREEN}Removing all pycache and pytest_cache${RESET}"
echo "${BLUE}#############################################################################################################${RESET}"
find . | grep -E "(__pycache__|\.pyc|\.pyo$$)" | xargs rm -rf || echo "${YELLOW}No pycache to remove.${RESET}"
find . | grep -E "(\.pytest_cache)" | xargs rm -rf || echo "${YELLOW}No pytest_cache to remove.${RESET}"

sleep 1
echo "${BLUE}#############################################################################################################${RESET}"
echo "                                        ${GREEN}Removing all docker scout cache${RESET}"
echo "${BLUE}#############################################################################################################${RESET}"
rm -rf ~/.docker/scout/sbom/* || true
docker scout cache prune -f || true

sleep 1
echo "${BLUE}#############################################################################################################${RESET}"
echo "                                        ${GREEN}Remove postgres/ mount.${RESET}"
echo "${BLUE}#############################################################################################################${RESET}"
rm -rf postgres/

sleep 1
echo "${BLUE}#############################################################################################################${RESET}"
echo "                                        ${GREEN}Check and remove the images' cache if exists.${RESET}"
echo "${BLUE}#############################################################################################################${RESET}"
docker system prune -a -f || echo "${YELLOW}No images' data to remove.${RESET}"
docker system df
echo "${BLUE}#############################################################################################################${RESET}"
echo "                                        ${RED}All Docker items were removed!${RESET}"
echo "${BLUE}#############################################################################################################${RESET}"
#### END ###
