#!/bin/bash


echo "Starting the application!"
echo "Building ..."
docker compose build --no-cache
echo "Building done!"
echo "Starting ..."
docker compose up db -d
for i in {1..10}
do
  echo "Waiting for database to start..."
  echo "Attempt $i"
  sleep 1
done
docker compose up backend -d
docker compose up adminer -d
echo "Starting done!"
#docker compose up frontend -d
#docker compose up nginx -d
