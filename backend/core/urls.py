from django.contrib import admin
from django.urls import include
from django.urls import path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from users.api import CustomObtainAuthToken

obtain_auth_token = CustomObtainAuthToken.as_view()

# Swagger
schema_view = get_schema_view(
    openapi.Info(
        title="API", default_version='v1', description="API",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="stomchuk34@gmail.com"),
        license=openapi.License(name="MIT License"),
    ),
    public=True,
    permission_classes=(permissions.IsAuthenticatedOrReadOnly,),
)

urlpatterns = [
    # Swagger
    path('swagger/', schema_view.with_ui(
        'swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui(
        'redoc', cache_timeout=0), name='schema-redoc'),

    # Django
    path('admin/', admin.site.urls),
    path('api/v1/', include('users.urls'), name='users'),
    path('api/v1/', include('tasks.urls'), name='tasks'),

    # rest_framework.authtoken
    path('api/v1/', include('rest_framework.urls'), name='rest_framework'),
    path('api/v1/token-auth/', obtain_auth_token, name='token-auth'),
]
