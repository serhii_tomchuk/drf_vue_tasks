import os
from pathlib import Path

from dotenv import load_dotenv

load_dotenv()

BASE_DIR = Path(__file__).resolve().parent.parent

SECRET_KEY = os.getenv('DJANGO_SECRET_KEY')

DEBUG = True  # os.getenv('DJANGO_DEBUG', True)

ALLOWED_HOSTS = ["*"] \
    if DEBUG is True else os.getenv('DJANGO_ALLOWED_HOSTS').split(',')

CORS_ALLOWED_ORIGINS = [
    "http://localhost:3000", "http://localhost:8000",
    "http://127.0.0.1:3000", "http://127.0.0.1:8000"] \
    if DEBUG is True else os.getenv('CORS_ALLOWED_ORIGINS').split(',')

CORS_ALLOW_CREDENTIALS = True
CORS_ALLOW_HEADERS = ['*'] \
    if DEBUG is True else os.getenv('CORS_ALLOWED_HEADERS').split(',')

CORS_ALLOW_METHODS = ['DELETE', 'GET', 'OPTIONS', 'PATCH', 'POST', 'PUT'] \
    if DEBUG is True else os.getenv('CORS_ALLOWED_METHODS').split(',')

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Swagger
    'drf_yasg',
    # My apps
    'users',
    'tasks',
    # Third-party apps
    'rest_framework',
    'rest_framework.authtoken'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware'
]

ROOT_URLCONF = 'core.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'core.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.getenv('POSTGRES_DB'),
        'USER': os.getenv('POSTGRES_USER'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD'),
        'HOST': os.getenv('POSTGRES_HOST'),
        'PORT': os.getenv('POSTGRES_PORT')
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation'
             '.UserAttributeSimilarityValidator', },
    {'NAME': 'django.contrib.auth.password_validation'
             '.MinimumLengthValidator', },
    {'NAME': 'django.contrib.auth.password_validation'
             '.CommonPasswordValidator', },
    {'NAME': 'django.contrib.auth.password_validation'
             '.NumericPasswordValidator', },
]

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_TZ = True
STATIC_URL = 'static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
AUTH_USER_MODEL = 'users.CustomUser'

# REST_FRAMEWORK
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticatedOrReadOnly',
        'rest_framework.permissions.IsAdminUser',
        'rest_framework.permissions.AllowAny'
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.TokenAuthentication'
    ],
    'DEFAULT_RENDERER_CLASSES': [
        'rest_framework.renderers.JSONRenderer'
    ],
    'DEFAULT_PARSER_CLASSES': [
        'rest_framework.parsers.JSONParser'
    ],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination'
                                '.PageNumberPagination'
}

# Swagger:
SWAGGER_SETTINGS = {
    'DEFAULT_GENERATOR_CLASS': 'drf_yasg.generators.OpenAPISchemaGenerator',
    'DEFAULT_INFO': 'core.urls.api_info',
    'DEFAULT_API_VERSION': 'v1',
    'DEFAULT_CONTACT': 'stomchuk34@gmail.com',
    'DEFAULT_LICENSE': 'MIT',
    'DEFAULT_TOS': 'http://www.google.com/policies/terms/',
    'DEFAULT_SCHEMES': ['http', 'https'],
    'DEFAULT_SERVERS': [
        {'url': 'http://localhost:8000', 'description': 'Local server'},
    ],
    'DEFAULT_SECURITY': [
        {'Basic': ['read', 'write']}, {'Token': ['read', 'write']},
    ],
    'DEFAULT_API_DESCRIPTION': 'API for the test task.',
    'DEFAULT_MODEL_DEPTH': 3,
    'DEFAULT_PAGINATOR_CLASS': 'rest_framework.pagination'
                               '.PageNumberPagination',
    'DEFAULT_PAGINATOR_GET_PARAMETER': 'page',
    'DEFAULT_PAGINATOR_PAGE_SIZE': 10,
    'DEFAULT_PAGINATOR_PAGE_SIZE_QUERY_PARAM': 'page_size',
    'DEFAULT_PAGINATOR_MAX_PAGE_SIZE': 100,
    'DEFAULT_FILTER_BACKENDS': [
        'django_filters.rest_framework.DjangoFilterBackend',
        'rest_framework.filters.OrderingFilter',
        'rest_framework.filters.SearchFilter'
    ],
    'DEFAULT_FILTER_PARAMETER_NAME': 'filter',
    'DEFAULT_EXCLUDE_PATHS': [],
    'DEFAULT_JSON_EDITOR': True,
    'DEFAULT_AUTO_SCHEMA_CLASS': 'drf_yasg.inspectors.SwaggerAutoSchema',
    'SECURITY_DEFINITIONS': {
        'Token': {'type': 'apiKey', 'name': 'Authorization', 'in': 'header'}
    },
    'LOGIN_URL': 'rest_framework:login',
    'LOGOUT_URL': 'rest_framework:logout',
    'USER_SERIALIZER': 'users.serializers.UserSerializer',
    'REFETCH_SCHEMA_WITH_AUTH': True,
}

# ACCOUNT_USER_MODEL_USERNAME_FIELD = None
# ACCOUNT_USER_MODEL_USERNAME_FIELD = None
# ACCOUNT_USER_MODEL_EMAIL_FIELD = 'email'
# ACCOUNT_USER_MODEL_EMAIL_FIELD = 'email'
# ACCOUNT_AUTHENTICATION_METHOD = 'email'
# ACCOUNT_EMAIL_REQUIRED = True
# ACCOUNT_USERNAME_REQUIRED = False
# ACCOUNT_UNIQUE_EMAIL = True
