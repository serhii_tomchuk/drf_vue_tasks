from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.schemas import coreapi as coreapi_schema
from rest_framework.viewsets import ModelViewSet
from users.models import CustomUser as User
from users.serializers import CustomAuthTokenSerializer
from users.serializers import UserSerializer


class CustomObtainAuthToken(ObtainAuthToken):
    # TODO: Move this to a authentication.py file
    serializer_class = CustomAuthTokenSerializer

    if coreapi_schema.is_enabled():
        schema = ManualSchema(
            fields=[
                coreapi.Field(
                    name="email",
                    required=True,
                    location='form',
                    schema=coreschema.String(
                        title="Email",
                        description="Valid email for authentication",
                    ),
                ),
                coreapi.Field(
                    name="password",
                    required=True,
                    location='form',
                    schema=coreschema.String(
                        title="Password",
                        description="Valid password for authentication",
                    ),
                ),
            ],
            encoding="application/json",
        )

    def post(self, request, *args, **kwargs):
        response = super(CustomObtainAuthToken,
                         self).post(request, *args, **kwargs)
        token = response.data['token']
        user = User.objects.get(auth_token=token)
        return response


class UserModelViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated, IsAdminUser]
    http_method_names = ['get', 'post', 'put', 'delete']
