from django.urls import path, include
from rest_framework import routers
from users.api import UserModelViewSet

users_router = routers.DefaultRouter()
users_router.register(r'users', UserModelViewSet)

urlpatterns = [
    path('', include(users_router.urls), name='users')
]
