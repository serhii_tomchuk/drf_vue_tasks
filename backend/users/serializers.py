from rest_framework import serializers

from users.models import CustomUser as User
from rest_framework.authtoken.models import Token

from rest_framework.authtoken.serializers import AuthTokenSerializer


class CustomAuthTokenSerializer(AuthTokenSerializer):

    username = None
    email = serializers.EmailField(label='Email')

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        if email and password:
            user = User.objects.filter(email=email).first()
            if user:
                if not user.check_password(password):
                    msg = 'Unable to log in with provided credentials.'
                    raise serializers.ValidationError(
                        msg, code='authorization')
            else:
                msg = 'Unable to log in with provided credentials.'
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = 'Must include "email" and "password".'
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs


class TokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = ('key',)


class UserSerializer(serializers.ModelSerializer):
    token = TokenSerializer(read_only=True)

    class Meta:
        model = User
        fields = (
            'id', 'email', 'first_name', 'last_name', 'is_active',
            'is_staff', 'is_superuser', 'created_at', 'updated_at', 'token'
        )
        read_only_fields = ('id', 'token',)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        try:
            token = Token.objects.get(user=instance)
        except Token.DoesNotExist:
            token = Token.objects.create(user=instance)
        instance.user_token = token.key
        data['token'] = instance.user_token
        return data
