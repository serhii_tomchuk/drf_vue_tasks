from rest_framework import serializers
from tasks.models import ToDoTask as Task

class TasksCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'
        read_only_fields = ('user', )

    def create(self, validated_data):
        user = self.context['request'].user
        task = Task.objects.create(user=user, **validated_data)
        return task


class TasksSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField()
    class Meta:
        model = Task
        fields = '__all__'
        read_only_fields = ('user', )
