from django.contrib import admin
from tasks.models import ToDoTask as Task


admin.site.register(Task)
