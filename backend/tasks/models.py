from django.db import models
from users.models import CustomUser as User


class ToDoTask(models.Model):
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='tasks',
        null=True, blank=True
    )
    title = models.CharField(max_length=100)
    description = models.TextField(max_length=5000, null=True, blank=True)
    completed = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    completed_at = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-created_at', '-updated_at', '-completed_at']
