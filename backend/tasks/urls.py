from django.urls import include, path
from rest_framework import routers
from tasks.api import TaskModelViewSet

tasks_router = routers.DefaultRouter()
tasks_router.register(r'tasks', TaskModelViewSet)

urlpatterns = [
    path('', include(tasks_router.urls), name='tasks')
]
