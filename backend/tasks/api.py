from rest_framework.viewsets import ModelViewSet

from rest_framework.permissions import IsAuthenticated, IsAdminUser
from tasks.serializers import TasksCreateSerializer
from tasks.models import ToDoTask as Task
from tasks.serializers import TasksSerializer


class TaskModelViewSet(ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TasksSerializer
    http_method_names = ['get', 'post', 'put', 'delete']
    permission_classes = [IsAuthenticated, IsAdminUser]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        return Task.objects.filter(user=self.request.user)

    def get_serializer_class(self):
        if self.action == 'list' or self.action == 'retrieve':
            return TasksSerializer
        return TasksCreateSerializer

    def get_permissions(self):
        if self.action == 'list' or self.action == 'retrieve':
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAuthenticated, IsAdminUser]
        return [permission() for permission in permission_classes]

    def get_serializer_context(self):
        context = super(TaskModelViewSet, self).get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_serializer_context(self):
        context = super(TaskModelViewSet, self).get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_serializer_context(self):
        context = super(TaskModelViewSet, self).get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_serializer_context(self):
        context = super(TaskModelViewSet, self).get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_serializer_context(self):
        context = super(TaskModelViewSet, self).get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_serializer_context(self):
        context = super(TaskModelViewSet, self).get_serializer_context
