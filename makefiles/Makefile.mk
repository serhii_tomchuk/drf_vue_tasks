#### Start ####
# Colors
RED = \033[0;31m
GREEN = \033[0;32m
CYAN = \033[0;36m
YELLOW = \033[0;33m
MAGENTA = \033[0;35m
NC = \033[0m # No Color
# Commands
run:
	@echo "Run"
	$(shell pwd)/scripts/starter.sh
	@echo "Run done"

hardkill:
	@echo "Kill"
	$(shell pwd)/scripts/hardkillDocker.sh
	@echo "Kill done"

hard-rebuild:
	@echo "Hard rebuild"
	@echo "$(shell pwd)"
	$(shell pwd)/scripts/hardKillDocker.sh
	$(shell pwd)/scripts/rm_db_logs.sh
	$(shell pwd)/scripts/starter.sh
	@echo "Hard rebuild done"

restart:
	@echo "Restart"
	docker compose down && docker compose up -d
	@echo "Restart done"

rebuild:
	@echo "Rebuild"
	docker compose up -d --build --force-recreate --remove-orphans --no-deps --renew-anon-volumes
	@echo "Rebuild done"

exec-to-db:
	@echo "Exec to db"
	docker exec -it db /bin/sh

exec-to-web:
	@echo "Exec to web"
	docker exec -it web /bin/bash

db-logs:
	@echo "DB logs"
	docker logs db -f

web-logs:
	@echo "Web logs"
	docker logs backend -f


help:
	@echo "${MAGENTA}############################################################################################${NC}"
	@echo "${MAGENTA}#------------------------------------Available commands:-----------------------------------#${NC}"
	@echo "${MAGENTA}############################################################################################${NC}"
	@echo "${CYAN}Usage:${NC}${YELLOW}\t make [command]${NC}"
	@echo "${GREEN}\trun${NC}\t\t - Run, start all containers."
	@echo "${GREEN}\thardkill${NC}\t - Hard kill, remove all containers, images, etc."
	@echo "${GREEN}\thard-rebuild${NC}\t - Hard rebuild, remove all containers, images, etc and start again."
	@echo "${GREEN}\texec-to-db${NC}\t - Exec to db, enter to db container."
	@echo "${GREEN}\texec-to-web${NC}\t - Exec to web, enter to web container."
	@echo "${GREEN}\tdb-logs${NC}\t - DB logs, show db logs."
	@echo "${GREEN}\tweb-logs${NC}\t - Web logs, show web logs."
	@echo "${GREEN}\thelp${NC}\t\t - Help"
### End ###